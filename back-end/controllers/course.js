const Course = require('../models/course')



// For User
module.exports.active = () => {
	return Course.find({isActive:true}).then(courses => courses)
}

// for Admin
module.exports.getAll = () => {
	return Course.find().then(courses => courses)
}

//old
// module.exports.getAll = () => {
// 	return Course.find({ isActive: true }).then(courses => courses)
// }

module.exports.add = (params) => {
	let course = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	})

	return course.save().then((course, err) => {
		return (err) ? false : true
	})
}

module.exports.get = (params) => {
	return Course.findById(params.courseId).then(course => course)
}

module.exports.update = (params) => {
	const updates = {
		name: params.name,
		description: params.description,
		price: params.price
	}

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}

module.exports.archive = (params) => {
	const updates = { isActive: false }

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}

module.exports.reactivate = (params) => {
	const activate = { isActive: true }

	return Course.findByIdAndUpdate(params.courseId, activate).then((doc, err) => {
		return (err) ? false : true
	})
}